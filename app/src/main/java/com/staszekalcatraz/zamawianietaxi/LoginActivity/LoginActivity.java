package com.staszekalcatraz.zamawianietaxi.LoginActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.staszekalcatraz.zamawianietaxi.MainActivity.MainActivity;
import com.staszekalcatraz.zamawianietaxi.R;
import com.staszekalcatraz.zamawianietaxi.RegisterActivity.RegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LoginActivity extends AppCompatActivity implements LoginActivityMVP.View {

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etPassword)
    EditText etPassword;

    private static final String TAG = "LoginActivity";

    private Unbinder unbind;
    private LoginActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("");
        presenter = new LoginActivityPresenter(this, this);
        unbind = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbind.unbind();
    }

    @Override
    public String returnLoginFieldString() {
        return etEmail.getText().toString();
    }

    @Override
    public void setLoginFieldError(int resId) {
        etEmail.setError(getString(resId));
    }

    @Override
    public String returnPasswordFieldString() {
        return etPassword.getText().toString();
    }

    @Override
    public void setPasswordFieldError(int resId) {
        etPassword.setError(getString(resId));
    }

    @OnClick(R.id.bLogin)
    @Override
    public void onLoginButtonClick() {
        presenter.onLoginButtonClick();
    }

    @Override
    public void openMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @OnClick(R.id.textZalozKonto)
    @Override
    public void openRegisterActivity() {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    @Override
    public Context returnContext() {
        return this;
    }

    @Override
    public void setError(int resId) {
        Toast.makeText(this, "Błąd", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
