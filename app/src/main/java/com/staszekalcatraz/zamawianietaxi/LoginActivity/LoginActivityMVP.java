package com.staszekalcatraz.zamawianietaxi.LoginActivity;

import android.content.Context;

/**
 * Created by krzysztofm on 27.02.2018.
 */

public interface LoginActivityMVP {
    interface Presenter {
        void onLoginButtonClick();
    }

    interface View {
        String returnLoginFieldString();

        void setLoginFieldError(int resId);

        String returnPasswordFieldString();

        void setPasswordFieldError(int resId);

        void onLoginButtonClick();

        void openMainActivity();

        void openRegisterActivity();

        Context returnContext();

        void setError(int resId);
    }
}
