package com.staszekalcatraz.zamawianietaxi.LoginActivity;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.staszekalcatraz.zamawianietaxi.R;

/**
 * Created by krzysztofm on 27.02.2018.
 */

public class LoginActivityPresenter implements LoginActivityMVP.Presenter {

    private static final String TAG = "LoginActivityPresenter";

    private FirebaseAuth mFirebaseAuth;

    private Context context;

    private LoginActivityMVP.View view;

    public LoginActivityPresenter(Context context, LoginActivityMVP.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void onLoginButtonClick() {
        String email = view.returnLoginFieldString();
        if (email.isEmpty()) {
            view.setLoginFieldError(R.string.uzupelnij_email);
            return;
        }

        String password = view.returnPasswordFieldString();
        if (password.isEmpty()) {
            view.setPasswordFieldError(R.string.uzupelnij_haslo);
            return;
        }

        if (password.length() < 6) {
            view.setPasswordFieldError(R.string.minimum_6_znakow);
            return;
        }
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseAuth.signInWithEmailAndPassword(view.returnLoginFieldString(), view.returnPasswordFieldString())
                .addOnCompleteListener((Activity) view.returnContext(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            view.openMainActivity();
                            return;
                        } else {//jezei cos jest nie tak to wyrzuca blad
                            view.setError(R.string.error);
                            return;
                        }
                    }
                });
    }
}
