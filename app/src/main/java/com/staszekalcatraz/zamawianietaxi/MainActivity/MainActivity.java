package com.staszekalcatraz.zamawianietaxi.MainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.staszekalcatraz.zamawianietaxi.LoginActivity.LoginActivity;
import com.staszekalcatraz.zamawianietaxi.MainActivity.TwojeZleceniaFragment.TwojeZleceniaFragment;
import com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.WszystkieZleceniaFragment;
import com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.utilsListaZlecenia.ZleceniaDataAdapter;
import com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.utilsListaZlecenia.ZlecenieModel;
import com.staszekalcatraz.zamawianietaxi.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity implements MainActivityMVP.View {

    private static final String TAG = "MainActivity";

    private MainActivityPresenter presenter;

    private List<ZlecenieModel> zlecenieModelList;

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;

    private Unbinder unbind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getSupportActionBar().setIcon(R.drawable.logo);
        this.setTitle("Delivery Expert");
        unbind = ButterKnife.bind(this);
        presenter = new MainActivityPresenter(this, this);
        presenter.checkUserAuth();

        BottomNavigationView navigation = findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;

                switch (item.getItemId()) {
                    case R.id.navigation_wszystkie_zlecenia:
                        selectedFragment = WszystkieZleceniaFragment.newInstance();
                        break;
                    case R.id.navigation_twoje_zlecenia:
                        selectedFragment = TwojeZleceniaFragment.newInstance();
                        break;
                }

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content, selectedFragment);
                fragmentTransaction.commit();

                return true;
            }
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, WszystkieZleceniaFragment.newInstance());
        transaction.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbind.unbind();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.checkUserAuth();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            presenter.onLogoutButtonClicked();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
    }
}
