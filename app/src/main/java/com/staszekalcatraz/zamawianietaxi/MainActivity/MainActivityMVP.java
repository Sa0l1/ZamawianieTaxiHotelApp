package com.staszekalcatraz.zamawianietaxi.MainActivity;


/**
 * Created by krzysztofm on 27.02.2018.
 */

public interface MainActivityMVP {
    interface Presenter {
        void checkUserAuth();

        void onLogoutButtonClicked();
    }

    interface View {
        void openLoginActivity();
    }
}
