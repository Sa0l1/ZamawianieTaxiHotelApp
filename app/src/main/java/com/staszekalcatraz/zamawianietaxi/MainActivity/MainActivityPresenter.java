package com.staszekalcatraz.zamawianietaxi.MainActivity;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by krzysztofm on 27.02.2018.
 */

public class MainActivityPresenter implements MainActivityMVP.Presenter {

    private static final String TAG = "MainActivityPresenter";

    private MainActivityMVP.View view;

    private Context context;


    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;

    public MainActivityPresenter(MainActivityMVP.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void checkUserAuth() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        //jezeli nie zalogowany, wyloguj
        if (mFirebaseUser == null) {
            view.openLoginActivity();
        }
    }

    @Override
    public void onLogoutButtonClicked() {
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser != null) {
            mFirebaseAuth = FirebaseAuth.getInstance();
            mFirebaseAuth.signOut();
            view.openLoginActivity();
        }
    }
}