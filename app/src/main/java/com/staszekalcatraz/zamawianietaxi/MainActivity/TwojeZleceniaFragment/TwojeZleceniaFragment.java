package com.staszekalcatraz.zamawianietaxi.MainActivity.TwojeZleceniaFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.staszekalcatraz.zamawianietaxi.MainActivity.MainActivity;
import com.staszekalcatraz.zamawianietaxi.MainActivity.TwojeZleceniaFragment.utilsTwojeZlecenia.MojeZleceniaDataAdapter;
import com.staszekalcatraz.zamawianietaxi.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TwojeZleceniaFragment extends Fragment implements TwojeZleceniaFragmentMVP.View {

    private static final String TAG = "TwojeZleceniaFragment";

    private TwojeZleceniaFragmentPresenter presenter;

    @BindView(R.id.rvMojaListaZlecen)
    RecyclerView rvMojaListaZlecen;

    public TwojeZleceniaFragment() {
        // Required empty public constructor
    }

    public static TwojeZleceniaFragment newInstance() {
        TwojeZleceniaFragment fragment = new TwojeZleceniaFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_twoje_zlecenia, container, false);
        ButterKnife.bind(this, view);
        presenter = new TwojeZleceniaFragmentPresenter(getActivity(), this);
        presenter.loadTwojeZlecenia();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setTwojeZleceniaRecyclerViewAdapter(MojeZleceniaDataAdapter mojeZleceniaDataAdapter) {
        //lista zlecen
        rvMojaListaZlecen.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvMojaListaZlecen.setLayoutManager(llm);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        rvMojaListaZlecen.setAdapter(mojeZleceniaDataAdapter);
        mojeZleceniaDataAdapter.notifyDataSetChanged();
    }
}
