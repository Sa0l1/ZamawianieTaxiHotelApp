package com.staszekalcatraz.zamawianietaxi.MainActivity.TwojeZleceniaFragment;

import com.staszekalcatraz.zamawianietaxi.MainActivity.TwojeZleceniaFragment.utilsTwojeZlecenia.MojeZleceniaDataAdapter;

/**
 * Created by krzysztofm on 01.03.2018.
 */

public interface TwojeZleceniaFragmentMVP {
    interface Presenter {
        void loadTwojeZlecenia();
    }

    interface View {
        void setTwojeZleceniaRecyclerViewAdapter(MojeZleceniaDataAdapter mojeZleceniaDataAdapter);
    }
}
