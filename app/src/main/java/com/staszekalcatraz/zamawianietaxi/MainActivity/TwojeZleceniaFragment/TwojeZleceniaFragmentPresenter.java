package com.staszekalcatraz.zamawianietaxi.MainActivity.TwojeZleceniaFragment;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.staszekalcatraz.zamawianietaxi.MainActivity.TwojeZleceniaFragment.utilsTwojeZlecenia.MojeZleceniaDataAdapter;
import com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.utilsListaZlecenia.ZlecenieModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by krzysztofm on 01.03.2018.
 */

public class TwojeZleceniaFragmentPresenter implements TwojeZleceniaFragmentMVP.Presenter {

    private static final String TAG = "TwojeZleceniaFragmentPr";

    private List<ZlecenieModel> twojeZleceniaModelList;

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private String mUserId;

    private Context context;
    private TwojeZleceniaFragmentMVP.View view;

    public TwojeZleceniaFragmentPresenter(Context context, TwojeZleceniaFragmentMVP.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void loadTwojeZlecenia() {
        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUserId = mFirebaseUser.getUid();
        mReference = mFirebaseDatabase.getReference("Taxowkarze");

        mFirebaseDatabase.getReference("Taxowkarze").child(mUserId).child("TwojeZlecenia").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                twojeZleceniaModelList = new ArrayList<>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    ZlecenieModel value = dataSnapshot1.getValue(ZlecenieModel.class);
                    Log.e(TAG, "onDataChange: " + value.getDaneKlienta());
                    ZlecenieModel zlecenieModel = new ZlecenieModel();

                    zlecenieModel.setData(value.getData());
                    Log.i(TAG, "onDataChange: " + value.getData());
                    zlecenieModel.setData(value.getData());
                    zlecenieModel.setDaneKlienta(value.getDaneKlienta());
                    zlecenieModel.setGodzina(value.getGodzina());
                    zlecenieModel.setOpis(value.getOpis());
                    zlecenieModel.setMiejscePrzyjazdu(value.getMiejscePrzyjazdu());
                    zlecenieModel.setKey(dataSnapshot1.getKey());

                    Log.e(TAG, "onDataChange: " + dataSnapshot1.getKey());

                    twojeZleceniaModelList.add(zlecenieModel);

                    MojeZleceniaDataAdapter zleceniaDataAdapter = new MojeZleceniaDataAdapter(context, twojeZleceniaModelList);
                    view.setTwojeZleceniaRecyclerViewAdapter(zleceniaDataAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError);
            }
        });
    }
}
