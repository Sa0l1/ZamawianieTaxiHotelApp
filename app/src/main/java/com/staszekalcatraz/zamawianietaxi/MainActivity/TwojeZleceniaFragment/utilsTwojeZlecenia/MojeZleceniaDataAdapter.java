package com.staszekalcatraz.zamawianietaxi.MainActivity.TwojeZleceniaFragment.utilsTwojeZlecenia;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.utilsListaZlecenia.ZlecenieModel;
import com.staszekalcatraz.zamawianietaxi.R;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by krzysztofm on 01.03.2018.
 */

public class MojeZleceniaDataAdapter extends RecyclerView.Adapter<MojeZleceniaDataAdapter.ViewHolder> {

    private static final String TAG = "MojeZleceniaDataAdapter";
    //firebase
    private FirebaseAuth mFirebaseAuth;

    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private String mUserId;

    private Context context;
    private List<ZlecenieModel> twojeZleceniaModelList;

    public MojeZleceniaDataAdapter(Context context, List<ZlecenieModel> twojeZleceniaModelList) {
        this.context = context;
        this.twojeZleceniaModelList = twojeZleceniaModelList;
    }

    @Override
    public MojeZleceniaDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.zlecenie_row, parent, false);
        MojeZleceniaDataAdapter.ViewHolder viewHolder = new MojeZleceniaDataAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MojeZleceniaDataAdapter.ViewHolder holder, final int position) {
        final ZlecenieModel zlecenieModel = twojeZleceniaModelList.get(position);
        holder.tvMiejscePrzyjazdu.setText(zlecenieModel.getMiejscePrzyjazdu());
        holder.tvGodzina.setText(zlecenieModel.getGodzina());

        String whoAdded = zlecenieModel.getData();
        holder.tvKtoDodal.setText(trimEmail(whoAdded));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final AlertDialog.Builder detailsDialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);
                detailsDialog.setTitle("Szczegóły");

                final String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

                LayoutInflater inflater = LayoutInflater.from(context);
                View details_layout = inflater.inflate(R.layout.single_zlecenie_dialog, null);
                detailsDialog.setView(details_layout);

                TextView tvKtoDodal = details_layout.findViewById(R.id.tvKtoDodal);
                TextView tvDaneKlienta = details_layout.findViewById(R.id.tvDaneKlienta);
                TextView tvMiejscePrzyjazdu = details_layout.findViewById(R.id.tvMiejscePrzyjazdu);
                TextView tvGodzinaWyjazdu = details_layout.findViewById(R.id.tvGodzinaWyjazdu);
                TextView tvOpis = details_layout.findViewById(R.id.tvOpis);

                Button bNavigate = details_layout.findViewById(R.id.bNavigate);
                bNavigate.setVisibility(View.VISIBLE);

                String whoAdded = zlecenieModel.getData();

                tvKtoDodal.setText(trimEmail(whoAdded));
                tvDaneKlienta.setText(zlecenieModel.getDaneKlienta());
                tvMiejscePrzyjazdu.setText(zlecenieModel.getMiejscePrzyjazdu());
                tvGodzinaWyjazdu.setText(zlecenieModel.getGodzina());
                tvOpis.setText(zlecenieModel.getOpis());
                Log.i(TAG, "onClick: " + zlecenieModel.getData());

                // Initialize Firebase Auth
                mFirebaseAuth = FirebaseAuth.getInstance();
                mFirebaseUser = mFirebaseAuth.getCurrentUser();
                zlecenieModel.setZakonczonoPrzez(mFirebaseUser.getEmail() + " Data: " + currentDateTimeString);

                bNavigate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + zlecenieModel.getMiejscePrzyjazdu());
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        try {
                            context.startActivity(mapIntent);
                        } catch (Throwable e) {
                            Toast.makeText(context, "Błąd", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                Log.e(TAG, "onClick: item key: " + zlecenieModel.getData());

                detailsDialog.setPositiveButton("Zakończ zlecenie", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog.Builder decyzjaDialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);
                        decyzjaDialog.setTitle("Czy wykonałeś to zlecenie?");

                        decyzjaDialog.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // Initialize Firebase Auth
                                mFirebaseAuth = FirebaseAuth.getInstance();
                                mFirebaseUser = mFirebaseAuth.getCurrentUser();
                                mFirebaseDatabase = FirebaseDatabase.getInstance();
                                mUserId = mFirebaseUser.getUid();

                                /**
                                 * dodaj do zakonczonych zlecen
                                 */
                                mReference = mFirebaseDatabase.getReference("ZakonczoneZlecenia");
                                mReference.push()
                                        .setValue(zlecenieModel)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                try {
                                                    mReference = mFirebaseDatabase.getReference("Taxowkarze")
                                                            .child(mUserId)
                                                            .child("TwojeZlecenia");
                                                    Log.e(TAG, "onSuccess: key: " + zlecenieModel.getData());
                                                    mReference.child(zlecenieModel.getKey()).removeValue();

                                                    //twojeZleceniaModelList.remove(position);
                                                    notifyDataSetChanged();
                                                    Toast.makeText(context, "Zlecenie zostało zakończone.", Toast.LENGTH_SHORT).show();
                                                } catch (Throwable e) {
                                                    Log.e(TAG, "onSuccess: " + e.getMessage());
                                                    Toast.makeText(context, "Ponów operacje.", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e(TAG, "onFailure: " + e.getMessage());
                                    }
                                });

                                /**
                                 * dodaj do zakonczonych zlecen zleceniodawcy
                                 */
                                /*mReference = mFirebaseDatabase.getReference("Users").child(zlecenieModel.getKey()).child("ZakonczoneZlecenia");
                                zlecenieModel.setZakonczonoPrzez(mFirebaseUser.getEmail() + "  " + currentDateTimeString);
                                mReference.push()
                                        .setValue(zlecenieModel)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                mReference = mFirebaseDatabase.getReference("ZakonczoneZlecenia");
                                                zlecenieModel.setZakonczonoPrzez(mFirebaseUser.getEmail() + "  " + currentDateTimeString);
                                                mReference.push().setValue(zlecenieModel);
                                                notifyDataSetChanged();
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                    }
                                });*/
                            }
                        }).setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        decyzjaDialog.create().show();
                    }
                }).setNegativeButton("Anuluj zlecenie", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog.Builder decyzjaDialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);
                        decyzjaDialog.setTitle("Czy chcesz anulować to zlecenie?");
                        decyzjaDialog.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // Initialize Firebase Auth
                                mFirebaseAuth = FirebaseAuth.getInstance();
                                mFirebaseUser = mFirebaseAuth.getCurrentUser();
                                mFirebaseDatabase = FirebaseDatabase.getInstance();
                                mUserId = mFirebaseUser.getUid();

                                mReference = mFirebaseDatabase.getReference("AktywneZlecenia");
                                mReference.push()
                                        .setValue(zlecenieModel)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                try {
                                                    Toast.makeText(context, "Anulowano zlecenie.", Toast.LENGTH_SHORT).show();
                                                    twojeZleceniaModelList.remove(position);
                                                    notifyDataSetChanged();

                                                    String key = zlecenieModel.getKey();
                                                    mReference = mFirebaseDatabase.getReference("Taxowkarze")
                                                            .child(mUserId)
                                                            .child("TwojeZlecenia");

                                                    mReference.child(zlecenieModel.getKey()).removeValue();
                                                    Log.e(TAG, "onSuccess: key: " + mReference.child(key));
                                                } catch (Exception e) {
                                                    Log.e(TAG, "onSuccess exception: ", e);
                                                    Toast.makeText(context, "Ponów operacje.", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e(TAG, "onFailure: " + e.getMessage());
                                    }
                                });
                            }
                        }).setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                notifyDataSetChanged();
                            }
                        });
                        decyzjaDialog.create().show();
                    }
                })/*.setNeutralButton("Odrzuć", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })*/;
                detailsDialog.create().show();
            }
        });
    }

    private String trimEmail(String whoAdded) {
        int index = whoAdded.indexOf('@');
        whoAdded = whoAdded.substring(0, index);

        return whoAdded;
    }

    @Override
    public int getItemCount() {
        return twojeZleceniaModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMiejscePrzyjazdu;
        private TextView tvGodzina;
        private TextView tvKtoDodal;

        public ViewHolder(View itemView) {
            super(itemView);

            tvKtoDodal = itemView.findViewById(R.id.tvKtoDodal);
            tvMiejscePrzyjazdu = itemView.findViewById(R.id.tvMiejscePrzyjazdu);
            tvGodzina = itemView.findViewById(R.id.tvGodzina);
        }
    }
}
