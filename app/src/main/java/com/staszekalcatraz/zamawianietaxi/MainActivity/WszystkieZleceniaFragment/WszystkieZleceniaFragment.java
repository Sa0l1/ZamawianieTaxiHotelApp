package com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.staszekalcatraz.zamawianietaxi.MainActivity.MainActivity;
import com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.utilsListaZlecenia.ZleceniaDataAdapter;
import com.staszekalcatraz.zamawianietaxi.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WszystkieZleceniaFragment extends Fragment implements WszystkieZleceniaFragmentMVP.View {

    private static final String TAG = "WszystkieZleceniaFragme";

    private WszystkieZleceniaFragmentPresenter presenter;

    @BindView(R.id.rvListaZlecen)
    RecyclerView rvListaZlecen;

    public WszystkieZleceniaFragment() {
        // Required empty public constructor
    }

    public static WszystkieZleceniaFragment newInstance() {
        WszystkieZleceniaFragment fragment = new WszystkieZleceniaFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wszystkie_zlecenia, container, false);
        ButterKnife.bind(this, view);
        presenter = new WszystkieZleceniaFragmentPresenter(getActivity(), this);

        //lista zlecen
        rvListaZlecen.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvListaZlecen.setLayoutManager(llm);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        presenter.loadListaZlecen();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setZleceniaRecyclerViewAdapter(ZleceniaDataAdapter zleceniaDataAdapter) {
        rvListaZlecen.setAdapter(zleceniaDataAdapter);
    }
}
