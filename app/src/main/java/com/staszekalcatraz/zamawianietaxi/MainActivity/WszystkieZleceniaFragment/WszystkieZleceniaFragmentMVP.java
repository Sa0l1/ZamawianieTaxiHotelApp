package com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment;

import com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.utilsListaZlecenia.ZleceniaDataAdapter;

/**
 * Created by krzysztofm on 02.03.2018.
 */

public interface WszystkieZleceniaFragmentMVP {

    interface Presenter {
        void loadListaZlecen();
    }

    interface View {
        void setZleceniaRecyclerViewAdapter(ZleceniaDataAdapter zleceniaDataAdapter);
    }

}

