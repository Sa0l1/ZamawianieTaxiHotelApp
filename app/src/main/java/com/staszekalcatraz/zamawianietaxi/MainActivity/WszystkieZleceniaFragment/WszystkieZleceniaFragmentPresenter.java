package com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.utilsListaZlecenia.ZleceniaDataAdapter;
import com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.utilsListaZlecenia.ZlecenieModel;
import com.staszekalcatraz.zamawianietaxi.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by krzysztofm on 02.03.2018.
 */

public class WszystkieZleceniaFragmentPresenter implements WszystkieZleceniaFragmentMVP.Presenter {

    private static final String TAG = "WszystkieZleceniaFragme";
    private static final String CHANNEL_ID = "";

    private Context context;

    private WszystkieZleceniaFragmentMVP.View view;

    private List<ZlecenieModel> zlecenieModelList;

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;

    public WszystkieZleceniaFragmentPresenter(Context context, WszystkieZleceniaFragmentMVP.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void loadListaZlecen() {
// Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mReference = mFirebaseDatabase.getReference("AktywneZlecenia");

        mReference.orderByChild("godzina").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                zlecenieModelList = new ArrayList<>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    ZlecenieModel value = dataSnapshot1.getValue(ZlecenieModel.class);
                    Log.e(TAG, "onDataChange: " + value.getDaneKlienta());
                    ZlecenieModel zlecenieModel = new ZlecenieModel();

                    zlecenieModel.setData(value.getData());
                    zlecenieModel.setDaneKlienta(value.getDaneKlienta());
                    zlecenieModel.setGodzina(value.getGodzina());
                    zlecenieModel.setOpis(value.getOpis());
                    zlecenieModel.setMiejscePrzyjazdu(value.getMiejscePrzyjazdu());
                    zlecenieModel.setKey(dataSnapshot1.getKey());

                    Log.e(TAG, "onDataChange: " + dataSnapshot1.getKey());

                    zlecenieModelList.add(zlecenieModel);

                    /*NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.logo)
                            .setContentTitle("Delivery expert")
                            .setContentText("Nowe zlecenie")
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(1, mBuilder.build());*/

                    ZleceniaDataAdapter zleceniaDataAdapter = new ZleceniaDataAdapter(context, zlecenieModelList);
                    view.setZleceniaRecyclerViewAdapter(zleceniaDataAdapter);
                    zleceniaDataAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError);
            }
        });
    }
}
