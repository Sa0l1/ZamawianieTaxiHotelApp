package com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.utilsListaZlecenia;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.staszekalcatraz.zamawianietaxi.R;

import java.util.List;

/**
 * Created by staszekalcatraz on 26/02/2018.
 */

public class ZleceniaDataAdapter extends RecyclerView.Adapter<ZleceniaDataAdapter.ViewHolder> {

    private static final String TAG = "ZleceniaDataAdapter";

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private String mUserId;

    private Context context;
    private List<ZlecenieModel> zlecenieModelList;

    public ZleceniaDataAdapter(Context context, List<ZlecenieModel> zlecenieModelList) {
        this.context = context;
        this.zlecenieModelList = zlecenieModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.zlecenie_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ZleceniaDataAdapter.ViewHolder holder, final int position) {
        final ZlecenieModel zlecenieModel = zlecenieModelList.get(position);
        holder.tvMiejscePrzyjazdu.setText(zlecenieModel.getMiejscePrzyjazdu());
        holder.tvGodzina.setText(zlecenieModel.getGodzina());

        String whoAdded = zlecenieModel.getData();
        holder.tvKtoDodal.setText(trimEmail(whoAdded));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                AlertDialog.Builder detailsDialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);

                LayoutInflater inflater = LayoutInflater.from(context);
                View details_layout = inflater.inflate(R.layout.single_zlecenie_dialog, null);
                detailsDialog.setView(details_layout);

                TextView tvKtoDodal = details_layout.findViewById(R.id.tvKtoDodal);
                TextView tvDaneKlienta = details_layout.findViewById(R.id.tvDaneKlienta);
                TextView tvMiejscePrzyjazdu = details_layout.findViewById(R.id.tvMiejscePrzyjazdu);
                TextView tvGodzinaWyjazdu = details_layout.findViewById(R.id.tvGodzinaWyjazdu);
                TextView tvOpis = details_layout.findViewById(R.id.tvOpis);

                tvKtoDodal.setText(trimEmail(zlecenieModel.getData()));
                tvDaneKlienta.setText(zlecenieModel.getDaneKlienta());
                tvMiejscePrzyjazdu.setText(zlecenieModel.getMiejscePrzyjazdu());
                tvGodzinaWyjazdu.setText(zlecenieModel.getGodzina());
                tvOpis.setText(zlecenieModel.getOpis());

                Log.e(TAG, "onClick: item key: data" + zlecenieModel.getData());

                //----------
                // Initialize Firebase Auth
                mFirebaseAuth = FirebaseAuth.getInstance();
                mFirebaseUser = mFirebaseAuth.getCurrentUser();
                mFirebaseDatabase = FirebaseDatabase.getInstance();
                mUserId = mFirebaseUser.getUid();

                mReference = mFirebaseDatabase.getReference("AktywneZlecenia");

               /* mReference.child(zlecenieModel.getKey()).child("zarezerwowane").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            String rezerwacja = dataSnapshot.getChildren().iterator().next().getValue(ZlecenieModel.class).getZarezerwowane();
                            Log.e(TAG, "onDataChange: rezerwacja: " + rezerwacja);
                        } catch (Exception e) {
                            Log.e(TAG, "onDataChange: exception e: " + e);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });*/

                /*if ((mReference.child(zlecenieModel.getKey()).child("zarezerwowane")).equals("nie")) {
                    Log.e(TAG, "onClick: zarezerwowane");
                    Log.e(TAG, "onClick: " + mReference.child(zlecenieModel.getKey()).child("zarezerwowane"));

                    return;
                } else {*/
                Log.e(TAG, "onClick: nie zarezerwowane");
                Log.e(TAG, "onClick: " + mReference.child(zlecenieModel.getKey()).child("zarezerwowane"));
                detailsDialog.setPositiveButton("Przyjmij zlecenie", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Initialize Firebase Auth
                        mFirebaseAuth = FirebaseAuth.getInstance();
                        mFirebaseUser = mFirebaseAuth.getCurrentUser();
                        mFirebaseDatabase = FirebaseDatabase.getInstance();
                        mUserId = mFirebaseUser.getUid();
                        mReference = mFirebaseDatabase.getReference("Taxowkarze").child(mUserId);
                        mReference.child("TwojeZlecenia")
                                .push()
                                .setValue(zlecenieModel)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        try {
                                            zlecenieModelList.remove(position);
                                            notifyDataSetChanged();
                                            Toast.makeText(context, "Dodano zlecenie.", Toast.LENGTH_SHORT).show();

                                            mReference = mFirebaseDatabase.getReference("AktywneZlecenia");
                                            mReference.child(zlecenieModel.getKey()).removeValue();
                                        }catch (Throwable e){
                                            Log.e(TAG, "onSuccess: "+e.getMessage() );
                                            Toast.makeText(context, "Ponów operacje.", Toast.LENGTH_SHORT).show();
                                        }
                                        /*mReference = mFirebaseDatabase.getReference("Users");
                                        mReference.child(zlecenieModel.getKey()).child("AktywneZlecenia").getKey();
                                        mReference.removeValue();
                                        Log.e(TAG, "onSuccess: key: " + mReference.child(key));*/
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e(TAG, "onFailure: " + e.getMessage());
                            }
                        });
                    }
                }).setNegativeButton("Odrzuć", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                detailsDialog.create().show();
            }
        });
    }

    private String trimEmail(String whoAdded) {

        int index = whoAdded.indexOf('@');
        whoAdded = whoAdded.substring(0, index);

        return whoAdded;
    }

    @Override
    public int getItemCount() {
        return zlecenieModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMiejscePrzyjazdu;
        private TextView tvGodzina;
        private TextView tvKtoDodal;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMiejscePrzyjazdu = itemView.findViewById(R.id.tvMiejscePrzyjazdu);
            tvGodzina = itemView.findViewById(R.id.tvGodzina);
            tvKtoDodal = itemView.findViewById(R.id.tvKtoDodal);
        }
    }
}
