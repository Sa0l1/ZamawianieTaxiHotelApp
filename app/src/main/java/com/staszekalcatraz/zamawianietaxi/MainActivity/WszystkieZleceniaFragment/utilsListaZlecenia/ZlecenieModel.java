package com.staszekalcatraz.zamawianietaxi.MainActivity.WszystkieZleceniaFragment.utilsListaZlecenia;

/**
 * Created by staszekalcatraz on 26/02/2018.
 */

public class ZlecenieModel {
    private String daneKlienta = "Brak";
    private String godzina;
    private String opis = "Brak";
    private String miejscePrzyjazdu;
    private String data;
    private String key = "";
    private String zakonczonoPrzez = "";
    private String zarezerwowane = "nie";

    public ZlecenieModel() {
    }

    public ZlecenieModel(String daneKlienta, String godzina, String opis, String miejscePrzyjazdu, String data, String key, String zakonczonoPrzez, String zarezerwowane) {
        this.daneKlienta = daneKlienta;
        this.godzina = godzina;
        this.opis = opis;
        this.miejscePrzyjazdu = miejscePrzyjazdu;
        this.data = data;
        this.key = key;
        this.zakonczonoPrzez = zakonczonoPrzez;
        this.zarezerwowane = zarezerwowane;
    }

    public String getZarezerwowane() {
        return zarezerwowane;
    }

    public void setZarezerwowane(String zarezerwowane) {
        this.zarezerwowane = zarezerwowane;
    }

    public String getDaneKlienta() {
        return daneKlienta;
    }

    public void setDaneKlienta(String daneKlienta) {
        this.daneKlienta = daneKlienta;
    }

    public String getMiejscePrzyjazdu() {
        return miejscePrzyjazdu;
    }

    public void setMiejscePrzyjazdu(String miejscePrzyjazdu) {
        this.miejscePrzyjazdu = miejscePrzyjazdu;
    }

    public String getGodzina() {
        return godzina;
    }

    public void setGodzina(String godzina) {
        this.godzina = godzina;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getZakonczonoPrzez() {
        return zakonczonoPrzez;
    }

    public void setZakonczonoPrzez(String zakonczonoPrzez) {
        this.zakonczonoPrzez = zakonczonoPrzez;
    }

    public String getData() {
        return data;
    }

    public ZlecenieModel setData(String data) {
        this.data = data;
        return this;
    }
}
