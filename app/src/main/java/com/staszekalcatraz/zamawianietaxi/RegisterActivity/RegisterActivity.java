package com.staszekalcatraz.zamawianietaxi.RegisterActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.staszekalcatraz.zamawianietaxi.MainActivity.MainActivity;
import com.staszekalcatraz.zamawianietaxi.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class RegisterActivity extends AppCompatActivity implements RegisterActivityMVP.View {

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.etRepeatPassword)
    EditText etRepeatPassword;

    private Unbinder unbind;

    private RegisterActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        presenter = new RegisterActivityPresenter(this, this);
        unbind = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbind.unbind();
    }

    @Override
    public String returnLoginFieldString() {
        return etEmail.getText().toString();
    }

    @Override
    public void setLoginFieldError(int resId) {
        etEmail.setError(getString(resId));
    }

    @Override
    public String returnPasswordFieldString() {
        return etPassword.getText().toString();
    }

    @Override
    public void setPasswordFieldError(int resId) {
        etPassword.setError(getString(resId));
    }

    @Override
    public String returnRepeatPasswordFieldString() {
        return etRepeatPassword.getText().toString();
    }

    @Override
    public void setRepeatPasswordFieldError(int resId) {
        etRepeatPassword.getText().toString();
    }

    @OnClick(R.id.bRegister)
    @Override
    public void onRegisterButtonClick() {
        presenter.onRegisterButtonClick();
    }

    @Override
    public void openMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public Context returnContext() {
        return this;
    }

    @Override
    public void setError(int resId) {
        Toast.makeText(this, "Błąd", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
