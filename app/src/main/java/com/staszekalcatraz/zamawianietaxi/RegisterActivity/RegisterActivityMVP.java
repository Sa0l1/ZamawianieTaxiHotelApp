package com.staszekalcatraz.zamawianietaxi.RegisterActivity;

import android.content.Context;

/**
 * Created by krzysztofm on 27.02.2018.
 */

interface RegisterActivityMVP {
    interface Presenter {
        void onRegisterButtonClick();

    }

    interface View {
        String returnLoginFieldString();

        void setLoginFieldError(int resId);

        String returnPasswordFieldString();

        void setPasswordFieldError(int resId);

        String returnRepeatPasswordFieldString();

        void setRepeatPasswordFieldError(int resId);

        void onRegisterButtonClick();

        void openMainActivity();

        Context returnContext();

        void setError(int resId);
    }
}
