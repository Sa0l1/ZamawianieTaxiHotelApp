package com.staszekalcatraz.zamawianietaxi.RegisterActivity;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.staszekalcatraz.zamawianietaxi.R;

/**
 * Created by krzysztofm on 27.02.2018.
 */

public class RegisterActivityPresenter implements RegisterActivityMVP.Presenter {

    private static final String TAG = "RegisterActivityPresent";

    private Context context;

    private RegisterActivityMVP.View view;
    private FirebaseAuth mFirebaseAuth;

    public RegisterActivityPresenter(Context context, RegisterActivityMVP.View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void onRegisterButtonClick() {
        String email = view.returnLoginFieldString();
        if (email.isEmpty()) {
            view.setLoginFieldError(R.string.uzupelnij_email);
            return;
        }

        String password = view.returnPasswordFieldString();
        if (password.isEmpty()) {
            view.setPasswordFieldError(R.string.uzupelnij_haslo);
            return;
        }

        if (password.length() < 6) {
            view.setPasswordFieldError(R.string.minimum_6_znakow);
            return;
        }

        String repeatPassword = view.returnRepeatPasswordFieldString();
        if (!repeatPassword.equals(password)) {
            view.setRepeatPasswordFieldError(R.string.hasla_sie_nie_zgadzaja);
            return;
        }

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseAuth.createUserWithEmailAndPassword(view.returnLoginFieldString(), view.returnPasswordFieldString())
                .addOnCompleteListener((Activity) view.returnContext(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            view.openMainActivity();
                            return;
                        } else {
                            view.setError(R.string.error);
                            return;
                        }
                    }
                });
    }
}
